import unittest
from selenium import webdriver
from Pages import *


class TestPages(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.get("http://www.amazon.com")

    def test_checkHomePage(self):
        main_page = MainPage(self.driver)
        self.assertTrue(main_page.check_page_loaded())

    def test_sign_in_button(self):
        page = MainPage(self.driver)
        print('inside sign in button test')

    def test_enterCredentialsandLogin(self):
        print('inside entercredentials test')
        page = MainPage(self.driver)
        login_Page = page.click_sign_in()
        login_Page.waitforelementvisible("ap_password")
        login_Page.UserLogin()
        login_Page.wait()
        print('inside entercredentials test')

    def tearDown(self):
            self.driver.close()


if __name__ == "__main__":
        suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
        unittest.TextTestRunner(verbosity=4).run(suite)
