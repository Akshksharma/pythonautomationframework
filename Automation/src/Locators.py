from selenium.webdriver.common.by import By


class HomePageLocators(object):
    Icon = (By.XPATH, ".//*[@id='nav-logo']/a[1]/span[contains(.,'Amazon')]")
    lnkAccountNlists = (By.XPATH, ".//*[@id='nav-link-accountList']/span[contains(text(),'Account & Lists')]")
    btnSignIn = (By.XPATH, "//div[@id='nav-flyout-ya-signin']//span[contains(.,'Sign in')]")


class LoginPageLocators(object):
    txtuserName = (By.ID, "ap_email")
    txtpassword = (By.ID, "ap_password")
    btnSignIn = (By.ID, "signInSubmit")




