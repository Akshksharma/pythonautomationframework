# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Selenium Automation Framework using Python
* Version 1


### How do I get set up? ###

Install PyCharm IDE

Install Python ans selenium

Clone the repository

Take the latest pull

### Contribution guidelines ###

* Writing tests :write test methods under TestRunner.py

* Other guidelines:
Working on CSV reader which will fetch TestData from CSV files
Analyzing different Reporting tools which can be implemented in this framework

### Who do I talk to? ###

* Reach me at akshayanilsharma9@gmail.com
